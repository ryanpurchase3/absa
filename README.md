# Absa

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 7.3.7.

## Development server

Run `npm start` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Build

Run `npm run build` or `npm run build:prod` to build the project. The build artifacts will be stored in the `dist/` directory.

## Test build with angular-http-server

Run `npm run serve:dist` to start a local server on port 9000 and open the default browser.

## NOTE:

Ensure the `mock-api-server` is running. The application provides no feedback if it's not running. In a live environment I would add an HTTP Inteceptor to catch all HTTP errors and provide the appropriate feedback.
