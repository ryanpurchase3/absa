import { State, Action, StateContext, Selector } from '@ngxs/store';
import { patch, updateItem } from '@ngxs/store/operators';
import { AccountsService } from '../service/accounts.service';
import { tap, catchError } from 'rxjs/operators';
import { of } from 'rxjs';
import { FetchAccounts, Withdraw, WithdrawSuccess } from './account.actions';
import { Account } from '../domain/Account';
import { MatSnackBar, MatSnackBarConfig } from '@angular/material';

export class AccountsStateModel {
  accounts: Account[];
  withdrawLimit: number;
  totalBalance: number;
}

@State<AccountsStateModel>({
  name: 'accounts',
  defaults: {
    accounts: null,
    withdrawLimit: -500,
    totalBalance: null
  }
})
export class AccountState {
  @Selector()
  static accounts(state: AccountsStateModel): Account[] {
    return state.accounts;
  }

  @Selector()
  static withdrawLimit(state: AccountsStateModel): number {
    return state.withdrawLimit;
  }

  @Selector()
  static totalBalance(state: AccountsStateModel): number {
    return state.accounts.map(a => +a.balance).reduce((a, b) => a + b);
  }

  constructor(private api: AccountsService, private snackBar: MatSnackBar) {}

  @Action(FetchAccounts)
  FetchAccounts(ctx: StateContext<AccountsStateModel>) {
    return this.api.fetchAccounts().pipe(
      tap(payload => {
        ctx.patchState({ accounts: payload });
      }),
      // TODO: Implement Typed errors that can be caught and handled globally using an HttpInteceptor.
      catchError(e => of(`Error fetching accounts: ${e}`))
    );
  }

  @Action(Withdraw)
  Withdraw(ctx: StateContext<AccountsStateModel>, { payload }: Withdraw) {
    // TODO: Hooked this up to the api to actually make a withdraw on the live data.
    const orignalAccount = ctx.getState().accounts.find(a => a.account_number === payload.account.account_number);

    ctx.setState(
      patch({
        accounts: updateItem<Account>(
          account => account.account_number === payload.account.account_number,
          patch({ balance: orignalAccount.balance - payload.amount })
        )
      })
    );

    return ctx.dispatch(new WithdrawSuccess());
  }

  @Action(WithdrawSuccess)
  WithdrawSuccess(ctx: StateContext<AccountsStateModel>) {
    const msg = 'Successful';
    const cfg: MatSnackBarConfig = {
      verticalPosition: 'top',
      duration: 2000
    };
    this.snackBar.open(msg, 'Withdraw', cfg);
  }
}
