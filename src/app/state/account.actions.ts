import { Account } from '../domain/Account';
import { WithdrawRequestPayload } from '../domain/WithdrawRequest';

export class FetchAccounts {
  static readonly type = '[Accounts] Fetch';
}

export class Withdraw {
  static readonly type = '[Accounts] Withdraw';
  constructor(public payload: WithdrawRequestPayload) {}
}

export class WithdrawSuccess {
  static readonly type = '[Accounts] Withdraw successful';
}
