import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA, ErrorStateMatcher } from '@angular/material';
import {
  FormBuilder,
  FormGroup,
  Validators,
  FormControl,
  AbstractControl,
  ValidatorFn,
  FormGroupDirective,
  NgForm
} from '@angular/forms';
import { Store } from '@ngxs/store';
import { Withdraw } from 'src/app/state/account.actions';
import { WithdrawRequestPayload } from 'src/app/domain/WithdrawRequest';
import { Account } from 'src/app/domain/Account';
import { AccountType } from 'src/app/domain/AccountType';
import { AccountState } from 'src/app/state/account.state';

export class ImmediateErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}
@Component({
  selector: 'app-withdraw-dialog',
  templateUrl: './withdraw-dialog.component.html',
  styleUrls: ['./withdraw-dialog.component.scss']
})
export class WithdrawDialogComponent implements OnInit {
  withdrawAmount: number;
  withdrawForm: FormGroup;
  account: Account;
  overdraftLimit: number;
  matcher = new ImmediateErrorStateMatcher();

  constructor(
    public dialogRef: MatDialogRef<WithdrawDialogComponent>,
    @Inject(MAT_DIALOG_DATA) public data: { account: Account },
    private store: Store,
    private fb: FormBuilder
  ) {}

  ngOnInit() {
    this.account = this.data.account;
    this.overdraftLimit = this.store.selectSnapshot(AccountState.withdrawLimit);
    this.withdrawForm = this.fb.group({
      amount: [
        null,
        [
          Validators.required,
          Validators.pattern('^[0-9]*$'),
          this.withdrawValidator(this.account.account_type, this.overdraftLimit)
        ]
      ]
    });
  }

  withdrawValidator(type: AccountType, limit: number): ValidatorFn {
    return (control: AbstractControl): { [key: string]: boolean } | null => {
      switch (type) {
        case AccountType.CHEQUE:
          return this.account.balance - control.value < limit ? { limitExceeded: true } : null;
        case AccountType.SAVINGS:
          return this.account.balance - control.value < 0 ? { limitExceeded: true } : null;
      }
      return null;
    };
  }

  submit() {
    if (this.withdrawForm.valid) {
      const payload: WithdrawRequestPayload = {
        account: this.account,
        amount: this.withdrawForm.get('amount').value
      };

      this.store
        .dispatch(new Withdraw(payload))
        .subscribe(() => this.dialogRef.close())
        .unsubscribe();
    }
  }
}
