import { Component, OnInit } from '@angular/core';
import { Store, Select } from '@ngxs/store';
import { FetchAccounts, Withdraw } from 'src/app/state/account.actions';
import { Observable } from 'rxjs';
import { Account } from 'src/app/domain/Account';
import { AccountState } from 'src/app/state/account.state';
import { AccountType } from 'src/app/domain/AccountType';
import { MatDialog } from '@angular/material';
import { WithdrawDialogComponent } from '../withdraw-dialig/withdraw-dialog.component';

@Component({
  selector: 'app-accounts',
  templateUrl: './accounts.component.html',
  styleUrls: ['./accounts.component.scss']
})
export class AccountsComponent implements OnInit {
  displayedColumns: string[] = ['account_number', 'account_type', 'balance', 'actions'];
  @Select(AccountState.accounts) dataSource$: Observable<Account[]>;
  @Select(AccountState.totalBalance) totalBalance$: Observable<number>;

  constructor(private store: Store, public dialog: MatDialog) {}

  ngOnInit() {
    this.store.dispatch(new FetchAccounts());
  }

  canWithdraw(account: Account): boolean {
    switch (account.account_type) {
      case AccountType.SAVINGS:
        return account.balance > 0;
        break;
      case AccountType.CHEQUE:
        return account.balance > this.store.selectSnapshot(AccountState.withdrawLimit);
        break;
    }
  }

  withdraw(account: Account) {
    const dialogRef = this.dialog.open(WithdrawDialogComponent, {
      width: '250px',
      data: { account }
    });

    dialogRef.afterClosed().subscribe(result => {
      console.log('Withdraw successful');
    });
  }
}
