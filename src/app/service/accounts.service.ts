import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Observable } from 'rxjs';
import { Account } from '../domain/Account';

@Injectable({
  providedIn: 'root'
})
export class AccountsService {
  private API_URL = environment.accountsGatewayUrl;

  constructor(private http: HttpClient) {}

  fetchAccounts(): Observable<Account[]> {
    return this.http.get<Account[]>(`${this.API_URL}/api/accounts`);
  }
}
