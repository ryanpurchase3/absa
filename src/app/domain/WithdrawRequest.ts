import { Account } from './Account';

export interface WithdrawRequestPayload {
  account: Account;
  amount: number;
}
