import { AccountType } from './AccountType';

export interface Account {
  account_number: string;
  account_type: AccountType;
  balance: number;
}
